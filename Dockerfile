FROM openjdk:8-jre-slim

RUN useradd --create-home appuser
WORKDIR /home/appuser
USER appuser

ADD ./build/libs/lemonade-supply-0.3.jar app.jar

CMD java -jar ${ADDITIONAL_OPTS} app.jar

EXPOSE 9002
